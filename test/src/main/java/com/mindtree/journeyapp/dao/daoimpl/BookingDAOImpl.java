package com.mindtree.journeyapp.dao.daoimpl;

import java.sql.*;
import java.util.ArrayList;

import com.mindtree.journeyapp.dao.BookingDAO;
import com.mindtree.journeyapp.entity.BookingDetails;
import com.mindtree.journeyapp.entity.City;
import com.mindtree.journeyapp.utility.DBUtil;

public class BookingDAOImpl implements BookingDAO {

	private Statement stat=null;
	private String sql;
	private Connection con=DBUtil.getconnection();

	public String getSource(String destination) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public ArrayList<BookingDetails> getBookingDetails(City destination) {
		
		ArrayList<BookingDetails> bookingDetails=new ArrayList<BookingDetails>();
		try {
			stat=con.createStatement();
			sql="select * from travelbooking where destination='"+destination.getName()+"'";
			ResultSet rs= stat.executeQuery(sql);
			while(rs.next())
			{
				BookingDetails bookingDetail=new BookingDetails();
				City sourceCity=new City();
				int bookingId=rs.getInt("BookingID");
				String source=rs.getString("source");
				sourceCity.setName(source);
				bookingDetail.setId(bookingId);
				bookingDetail.setSource(sourceCity);
				bookingDetails.add(bookingDetail);

			}
			rs.close();
			DBUtil.closeConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		return bookingDetails;
	}

}

package com.mindtree.journeyapp.dao;

import java.util.ArrayList;


import com.mindtree.journeyapp.entity.BookingDetails;
import com.mindtree.journeyapp.entity.City;


public interface BookingDAO {
	
	public String getSource(String destination);

	public 	ArrayList <BookingDetails> getBookingDetails(City destination);

}

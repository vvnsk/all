package com.mindtree.journeyapp.entity;

public class BookingDetails {
    private int id;
	private City source;
	private City destination;
	private Vehicle vehicletype;
	private long phoneno;
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + ((vehicletype == null) ? 0 : vehicletype.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BookingDetails other = (BookingDetails) obj;
		if (id != other.id)
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		if (vehicletype == null) {
			if (other.vehicletype != null)
				return false;
		} else if (!vehicletype.equals(other.vehicletype))
			return false;
		return true;
	}
	public BookingDetails(int id, City source, City destination, Vehicle vehicletype, long phoneno) {
		super();
		this.id = id;
		this.source = source;
		this.destination = destination;
		this.vehicletype = vehicletype;
		this.phoneno = phoneno;
	}
	public BookingDetails() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public City getSource() {
		return source;
	}
	public void setSource(City source) {
		this.source = source;
	}
	public City getDestination() {
		return destination;
	}
	public void setDestination(City destination) {
		this.destination = destination;
	}
	public Vehicle getVehicletype() {
		return vehicletype;
	}
	public void setVehicletype(Vehicle vehicletype) {
		this.vehicletype = vehicletype;
	}
	public long getPhoneno() {
		return phoneno;
	}
	public void setPhoneno(long phoneno) {
		this.phoneno = phoneno;
	}
	
	

}

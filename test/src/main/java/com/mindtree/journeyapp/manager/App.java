package com.mindtree.journeyapp.manager;

import java.util.*;

import com.mindtree.journeyapp.entity.BookingDetails;
import com.mindtree.journeyapp.entity.City;
import com.mindtree.journeyapp.service.BookingService;
import com.mindtree.journeyapp.service.serviceimpl.BookingServiceimpl;

public class App {
	
	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		String name;
		City destination;
		BookingService bookingService=new BookingServiceimpl();
		System.out.println("enter destination");
		name=sc.next();
		final String s1=" ";
		destination=new City();
		destination.setName(name);
		List <BookingDetails> li=bookingService.getBookingDetails(destination);
		if(li.isEmpty())
		{
			System.out.println("no details found");
		}
		else
		{
			System.out.println("booking id       source");
			System.out.println("\n");
			for(BookingDetails b:li )
			{
				System.out.println(b.getId()+"\t\t"+b.getSource().getName());
				
			}
			
		}
		sc.close();
		
	}

//	public static Object main(String string)
//	{
//		
//		
//		
//	}
	
	

}

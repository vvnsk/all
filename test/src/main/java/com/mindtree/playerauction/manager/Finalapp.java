package com.mindtree.playerauction.manager;

import java.util.*;

import com.mindtree.playerauction.exception.InvalidHighestScoreException;
import com.mindtree.playerauction.services.Playerservice;
import com.mindtree.playerauction.services.serviceimpl.PlayerServiceimpl;

public class Finalapp {
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("1.add player");
		System.out.println("2.display player");
		System.out.println("3.exit");
		System.out.println("enter your choice");
		int choice=sc.nextInt();
		if(choice==1)
		{
			Playerservice ps=new PlayerServiceimpl();
			System.out.println("enter the player name");
			String player_name=sc.next();
			System.out.println("enter category");
			String category=sc.next();
			System.out.println("enter highest score");
			int highestscore=sc.nextInt();
			try {
				ps.highestscore(highestscore);
			}
			catch (InvalidHighestScoreException e)
			{
				System.out.println(e.getMessage());
				System.exit(0);
			}
			
			System.out.println("enter best figure");
			String bestfigure=sc.next();
			System.out.println("enter team name");
			String teamname=sc.next();
			
			ps.addplayerService(player_name,category,highestscore,bestfigure,teamname);
			
		}
		if(choice==2)
		{
			System.out.println("enter the team name");
			String team_ans=sc.next();
			Playerservice ps=new PlayerServiceimpl();
			ps.displayService(team_ans);
			
		}
		
	}

}

package com.mindtree.playerauction.dao;

import java.util.*;

public interface PlayerDAO {

	public int addPlayer(String player_name, String category, int highestscore, String bestfigure, String teamname);
	public int getPlayerNumber();
	public void display(String teamname);
	

}

package com.mindtree.playerauction.dao.daoimpl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mindtree.journeyapp.utility.DBUtil;
import com.mindtree.playerauction.dao.PlayerDAO;


public class PlayerDAOimpl implements PlayerDAO{
	
	Connection con=DBUtil.getconnection();	

	public int addPlayer(String player_name, String category, int highestscore, String bestfigure, String teamname) {
		
String sql="insert into player(player_name,category,highestscore,bestfigure)values(?,?,?,?)";
  java.sql.PreparedStatement ps1;
  
      try {
		ps1=con.prepareStatement(sql);
		ps1.setString(1, player_name);
		ps1.setString(2,category);
		ps1.setInt(3,highestscore);
		ps1.setString(4,bestfigure);
		ps1.executeUpdate();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}


	addToTeamPlayer(getPlayerNumber(),getTeamId(teamname));
      
	
		return getPlayerNumber();
		
	}

	public void addToTeamPlayer(int playerNumber, int teamId) 
	{
	  String sql4="insert into team_player values(?,?)";
	  java.sql.PreparedStatement ps4;
	  
	  try {
		ps4=con.prepareStatement(sql4);
		ps4.setInt(1,playerNumber);
		ps4.setInt(2,teamId);
	    ps4.executeUpdate();
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  
	  
		
		
		
	}

	public int getTeamId(String teamname) 
	{
	   int teamId=0;
	   String sql1="select team_id from team where Team_name='"+teamname+"'";
       		java.sql.PreparedStatement ps2;
       		
       		try {
				ps2=con.prepareStatement(sql1);
				ResultSet rs=ps2.executeQuery();
	       		while(rs.next())
	       		{
	       			teamId=rs.getInt(1);
	       		}
			} 
       		catch (SQLException e) {
				
				e.printStackTrace();
			}
       		
       	return teamId;	
		
	}

	public int getPlayerNumber() {

		int playerNumber=0;
		String sql2="select player_no from player order by player_no desc limit 1";
		java.sql.PreparedStatement ps3;
		
		try {
			ps3=con.prepareStatement(sql2);
			ResultSet rs=ps3.executeQuery();
			while(rs.next())
				playerNumber=rs.getInt(1);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return playerNumber;
	}

	public void display(String teamname)
	
	{
		
		int team_id=getTeamId(teamname);
		
		String sql6="select player_name,category from player where player_no in(select player_no from team_player where team_id=?)";
		java.sql.PreparedStatement ps5;
		
		try {
			ps5=con.prepareStatement(sql6);
			ps5.setInt(1,team_id);
			ResultSet rs=ps5.executeQuery();
			System.out.println("playername\t category");
			while(rs.next())
			{
				System.out.println(rs.getString(1)+"\t\t"+rs.getString(2));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

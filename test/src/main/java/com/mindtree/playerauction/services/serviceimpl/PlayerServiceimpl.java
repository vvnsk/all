package com.mindtree.playerauction.services.serviceimpl;


import com.mindtree.playerauction.dao.PlayerDAO;
import com.mindtree.playerauction.dao.daoimpl.PlayerDAOimpl;
import com.mindtree.playerauction.exception.InvalidHighestScoreException;
import com.mindtree.playerauction.services.Playerservice;

public class PlayerServiceimpl implements Playerservice {

	PlayerDAO accessDao=new PlayerDAOimpl();

	public void addplayerService(String player_name, String category, int highestscore, String bestfigure,String teamname)
	{

		int playerno=accessDao.addPlayer(player_name,category,highestscore,bestfigure,teamname);
		System.out.println("player is added at"+playerno);

	}

	public void displayService(String teamname)
	{
		PlayerDAO pd=new PlayerDAOimpl();
		pd.display(teamname);
	}

	public void highestscore(int highestscore)throws InvalidHighestScoreException 
	{
		if(highestscore<50)
		{
			throw new InvalidHighestScoreException("score invalid");
		}

	}



}

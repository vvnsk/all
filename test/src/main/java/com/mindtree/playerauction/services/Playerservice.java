package com.mindtree.playerauction.services;

import com.mindtree.playerauction.exception.InvalidHighestScoreException;

public interface Playerservice {

	public void addplayerService(String player_name, String category, int highestscore, String bestfigure,String teamname);

	public void displayService(String team_ans);
	
	void highestscore(int highestscore) throws InvalidHighestScoreException;

}

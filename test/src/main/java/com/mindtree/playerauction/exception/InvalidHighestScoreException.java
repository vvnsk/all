package com.mindtree.playerauction.exception;

public class InvalidHighestScoreException extends Exception 
{
	public InvalidHighestScoreException(String s)
    {
        super(s);
    }	

}

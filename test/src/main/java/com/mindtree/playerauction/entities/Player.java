package com.mindtree.playerauction.entities;

public class Player {
	
	private String player_name;
	private String category;
	private int highestScore;
	private String bestfigure;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bestfigure == null) ? 0 : bestfigure.hashCode());
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + highestScore;
		result = prime * result + ((player_name == null) ? 0 : player_name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (bestfigure == null) {
			if (other.bestfigure != null)
				return false;
		} else if (!bestfigure.equals(other.bestfigure))
			return false;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (highestScore != other.highestScore)
			return false;
		if (player_name == null) {
			if (other.player_name != null)
				return false;
		} else if (!player_name.equals(other.player_name))
			return false;
		return true;
	}
	public Player(String player_name, String category, int highestScore, String bestfigure) {
		super();
		this.player_name = player_name;
		this.category = category;
		this.highestScore = highestScore;
		this.bestfigure = bestfigure;
	}
	public Player() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getPlayer_name() {
		return player_name;
	}
	public void setPlayer_name(String player_name) {
		this.player_name = player_name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public int getHighestScore() {
		return highestScore;
	}
	public void setHighestScore(int highestScore) {
		this.highestScore = highestScore;
	}
	public String getBestfigure() {
		return bestfigure;
	}
	public void setBestfigure(String bestfigure) {
		this.bestfigure = bestfigure;
	}



}

package com.mindtree.playerauction.entities;

public class Team {
	
	private int Team_id;
	private String Team_name;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Team_id;
		result = prime * result + ((Team_name == null) ? 0 : Team_name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Team other = (Team) obj;
		if (Team_id != other.Team_id)
			return false;
		if (Team_name == null) {
			if (other.Team_name != null)
				return false;
		} else if (!Team_name.equals(other.Team_name))
			return false;
		return true;
	}
	public Team(int team_id, String team_name) {
		super();
		Team_id = team_id;
		Team_name = team_name;
	}
	public Team() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getTeam_id() {
		return Team_id;
	}
	public void setTeam_id(int team_id) {
		Team_id = team_id;
	}
	public String getTeam_name() {
		return Team_name;
	}
	public void setTeam_name(String team_name) {
		Team_name = team_name;
	}
	

}

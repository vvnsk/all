package com.mindtree.bookstore.service;

import java.util.ArrayList;

import com.mindtree.bookstore.entity.Book;
import com.mindtree.bookstore.entity.Purchase;

public interface BookService {

	public ArrayList<Book> displayBooks();
		
		public ArrayList<Book> displayBooks(Book category);
		
		public void purchase(Purchase book);
	


}

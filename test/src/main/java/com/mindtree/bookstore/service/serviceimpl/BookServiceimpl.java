package com.mindtree.bookstore.service.serviceimpl;

import java.util.ArrayList;

import com.mindtree.bookstore.dao.BookStoreDAO;
import com.mindtree.bookstore.dao.daiimpl.BookStoreDAOimpl;
import com.mindtree.bookstore.entity.Book;
import com.mindtree.bookstore.entity.Purchase;
import com.mindtree.bookstore.service.BookService;

public class BookServiceimpl implements BookService {
	
private BookStoreDAO bookdao=new BookStoreDAOimpl();
	
	public ArrayList<Book> displayBooks() {
		
		return bookdao.displayBookCategory();
	}

	public ArrayList<Book> displayBooks(Book category) {
		// TODO Auto-generated method stub
		return bookdao.displayBooks(category);
	}	
	
	public void purchase(Purchase book ){
		  bookdao.purchase(book);
	}
}
	



package com.mindtree.bookstore.dao.daiimpl;

import java.sql.*;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.mindtree.bookstore.dao.BookStoreDAO;
import com.mindtree.bookstore.entity.Book;
import com.mindtree.bookstore.entity.Purchase;
import com.mindtree.bookstore.utility.DBUtil;
import com.mysql.jdbc.Connection;

public class BookStoreDAOimpl implements BookStoreDAO{

		private Statement st=null;
		 
		private String sql;
		private Connection con=(Connection) DBUtil.getconnection();
		//display category
		public ArrayList<Book> displayBookCategory() 
		{
			// TODO Auto-generated method stub
			ArrayList<Book> bookdetails=new ArrayList<Book>();
			try
			{
				st=con.createStatement();
				sql ="select distinct Category from book order by Category";
				ResultSet rs= st.executeQuery(sql);
				while(rs.next())
				{
					Book b = new Book();
					String categoryname=rs.getString("Category");
					b.setCategoryName(categoryname);
					bookdetails.add(b);
				}
				rs.close();
				 
			} catch (SQLException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return bookdetails;
		}
		//display books based on category
		public ArrayList<Book> displayBooks(Book category) {
	 
			ArrayList<Book> bookdetails=new ArrayList<Book>();
			try
			{
				st=con.createStatement();
				sql ="select Book_id,Book_name,Author_name,Publisher,price from Book where Category='"+category.getCategoryName()+"'";
				ResultSet rs= st.executeQuery(sql);
				while(rs.next())
				{
					Book b = new Book();
					int bookId=rs.getInt("Book_id");
					String bookName=rs.getString("Book_name");
					String authorName=rs.getString("Author_name");
					String publisher=rs.getString("Publisher");
					int price=rs.getInt("Price");
					b.setBookId(bookId);
					b.setBookName(bookName);
					b.setAuthorname(authorName);
					b.setPublisherName(publisher);
					b.setPrice(price);
					bookdetails.add(b);
				}
				rs.close();
				 
			} catch (SQLException e) 
			{ 
				e.printStackTrace();
			}
			
			return bookdetails;

		}
		//purchase
		public void purchase(Purchase p ) {
			// TODO Auto-generated method stub
			System.out.println(p.getBook().getBookId());
			PreparedStatement pstmt=null;
			int price=0;
			try
			{
				st=con.createStatement();
				sql ="select price from Book where Book_id='"+p.getBook().getBookId()+"'";
				ResultSet rs= st.executeQuery(sql);
				while(rs.next()){
					price=rs.getInt("price");
				}
				java.util.Date d = new java.util.Date();

				java.sql.Date  sqlDate = new java.sql.Date(d.getTime());
				  
				pstmt=con.prepareStatement("insert into Purchase(Book_id,Customer_name,Customer_mobileno,Purchase_date,Amount) values(?,?,?,?,?)");  
				pstmt.setObject(1,p.getBook().getBookId());
				pstmt.setString(2,p.getCustomerName());
				pstmt.setString(3,p.getCustomerMobileNo());
				pstmt.setDate(4, sqlDate);
				pstmt.setLong(5, price);
				
				System.out.println("Book successfully purchased");
				System.out.println("Purchase number: "+p.getPurchaseId());
				System.out.println("purchase date: "+ sqlDate);
				System.out.println("purchase amout: "+price);
				rs.close();
				 
			} catch (Exception e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
		 
		}
				 
	}




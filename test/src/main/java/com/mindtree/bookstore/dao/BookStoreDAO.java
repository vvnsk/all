package com.mindtree.bookstore.dao;

import java.util.ArrayList;

import com.mindtree.bookstore.entity.Book;
import com.mindtree.bookstore.entity.Purchase;

public interface BookStoreDAO {
		
		public ArrayList<Book> displayBookCategory();
		public ArrayList<Book> displayBooks(Book category);
		
		public void purchase(Purchase book);

	}



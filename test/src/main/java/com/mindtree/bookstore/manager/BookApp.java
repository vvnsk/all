package com.mindtree.bookstore.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.mindtree.bookstore.entity.Book;
import com.mindtree.bookstore.entity.Purchase;
import com.mindtree.bookstore.service.BookService;
import com.mindtree.bookstore.service.serviceimpl.BookServiceimpl;

public class BookApp {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BookService bookservice= new BookServiceimpl();
		do
		{
			Scanner sc = new Scanner(System.in);
			System.out.println("book stores");
			System.out.println("--------------------------");
			System.out.println("1. Display Book details");
			System.out.println("2. purchase a book");
			System.out.println("3. exit");
			System.out.println("enter the choice");
			int temp=sc.nextInt();
			 
			switch (temp) 
			{
				case 1:
				{	System.out.println("Avaliable Book categories");
					System.out.println("--------------------------");
					
					ArrayList<Book> ui=new ArrayList<Book>();
					ui=bookservice.displayBooks();
					System.out.println("Enter the Category");
									
					System.out.println(ui);
					Book b = new Book();
					String catergoryName=sc.next();
					b.setCategoryName(catergoryName);
					List<Book> details=bookservice.displayBooks(b);
					if(details.isEmpty()){
						System.out.println("books not found");
					}
					else{
						for (Book book : details) {
							System.out.println(book.getBookId()+" "+ book.getBookName()+" "+book.getAuthorname()+" "+book.getPublisherName()+" "+book.getPrice());
						}
					}
					break;
				}
				case 2:{
						Purchase p = new Purchase();
						Book b = new Book();
						System.out.println("----------------");
						System.out.println("Purchase Boook");
						System.out.println("----------------");
						System.out.print("Enter book id: ");
						int bookid=sc.nextInt();
						b.setBookId(bookid);						
						p.setBook(b);
						System.out.println(p.getBook().getBookId());
						System.out.print("Enter customer name: ");
						String CustomerName=sc.next();
						p.setCustomerName(CustomerName);
						System.out.println();
						String CustomerMobileNo=sc.next();
						p.setCustomerMobileNo(CustomerMobileNo);
						
						System.out.println("pp"+p);
						bookservice.purchase(p);
						break;
				}
				default:
						System.out.println("enter 1 or 2 or 3 options");
						
			}
		}while(true);
	}

}

package com.mindtree.bookstore.entity;

import java.util.*;

public class Purchase {
	
	private Book book;
	private String customerName;
	private String customerMobileNo;
	private Date purchaseDate;
	private int amount;
	private int purchaseId;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + amount;
		result = prime * result + ((book == null) ? 0 : book.hashCode());
		result = prime * result + ((customerMobileNo == null) ? 0 : customerMobileNo.hashCode());
		result = prime * result + ((customerName == null) ? 0 : customerName.hashCode());
		result = prime * result + ((purchaseDate == null) ? 0 : purchaseDate.hashCode());
		result = prime * result + purchaseId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Purchase other = (Purchase) obj;
		if (amount != other.amount)
			return false;
		if (book == null) {
			if (other.book != null)
				return false;
		} else if (!book.equals(other.book))
			return false;
		if (customerMobileNo == null) {
			if (other.customerMobileNo != null)
				return false;
		} else if (!customerMobileNo.equals(other.customerMobileNo))
			return false;
		if (customerName == null) {
			if (other.customerName != null)
				return false;
		} else if (!customerName.equals(other.customerName))
			return false;
		if (purchaseDate == null) {
			if (other.purchaseDate != null)
				return false;
		} else if (!purchaseDate.equals(other.purchaseDate))
			return false;
		if (purchaseId != other.purchaseId)
			return false;
		return true;
	}
	public Purchase(Book book, String customerName, String customerMobileNo, Date purchaseDate, int amount,
			int purchaseId) {
		super();
		this.book = book;
		this.customerName = customerName;
		this.customerMobileNo = customerMobileNo;
		this.purchaseDate = purchaseDate;
		this.amount = amount;
		this.purchaseId = purchaseId;
	}
	public Purchase() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerMobileNo() {
		return customerMobileNo;
	}
	public void setCustomerMobileNo(String customerMobileNo) {
		this.customerMobileNo = customerMobileNo;
	}
	public Date getPurchaseDate() {
		return purchaseDate;
	}
	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getPurchaseId() {
		return purchaseId;
	}
	public void setPurchaseId(int purchaseId) {
		this.purchaseId = purchaseId;
	}
	
	

}

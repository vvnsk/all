package test;

import java.util.Arrays;
import java.util.Scanner;

public class matrix {
	public static void main(String args[])
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("enter rows and columns");
		int n=sc.nextInt();
		int m=sc.nextInt();
		System.out.println("enter matrix elements");
		int[][] a=new int[n][m];
		for(int i=0;i<n;i++)
		{
			for(int j=0;j<m;j++)
			{
				a[i][j]=sc.nextInt();
			}

		}

		int[] res=arrangeElements(a);
		for(int i=0;i<res.length;i++)
		{
			System.out.print(res[i]+",");
		}
		sc.close();
	}
	
	public static int [ ] arrangeElements(int[][] a) {
		int n=a.length;
		int m=a[0].length;
//		System.out.println("m value is"+m);
		int arr[]=new int[n*m],k=0;
		int res[]=new int[n*m];
		int size=arr.length;
		for(int i=0;i<a.length;i++)
		{
			for(int j=0;j<a[i].length;j++)
			{
			   arr[k]=a[i][j];
			   k++;
			   
			}

		}
		Arrays.sort(arr);
		
		int mid=size/2;
		if(size%2==0)
		{
			mid=mid-1;
		}
		res[mid]=arr[0];
		int right=mid+1;
		int left=mid-1;
		for(int i=1;i<size;i++)
		{
			if(i%2==0)
			{
				
				res[left]=arr[i];
				left--;
			}
			else{
				
				res[right]=arr[i];
				right++;
			}
		}

		return  res; 

	}
		
	

}

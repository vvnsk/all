package test;

import java.util.Scanner;

public class fibonacci {
	public static void main(String args[])
	{
		System.out.println("enter the required number");
		Scanner sc=new Scanner(System.in);
		int k=sc.nextInt();
	    System.out.println(fib(k-1));
	    sc.close();
		
	}
	
	static int fib(int k)
	{
		if(k<=1)
		{
			return 1;
		}
		else
		{
			return fib(k-1)+fib(k-2);
		}
	}

}

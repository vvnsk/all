package com.mindtree.events.dao.DAOImpl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.mindtree.events.dao.EventDAO;
import com.mindtree.events.entities.Event1;

public class EventDAOImpl implements EventDAO {

	public void addRace(Event1 e) 
	{
		
		Configuration cfg=new Configuration();  
	    cfg.configure("hibernate.cfg.xml");
	    SessionFactory factory=cfg.buildSessionFactory();  
	    Session session=factory.openSession();  
	    Transaction t=session.beginTransaction();  
	    session.persist(e);
	    t.commit();
	    session.close();  
	    System.out.println("successfully saved");
	}

	public List<Event1> display() 
	{
		SessionFactory sessionFactory=new Configuration().configure().buildSessionFactory();
		Session session=sessionFactory.openSession();
		session.beginTransaction();
		Query query=session.createQuery("from Event1");
		List<Event1> list1 = query.list();
		return(list1);  
		
		
	}
	

}

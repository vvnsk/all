package com.mindtree.events.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.mindtree.events.entities.Event1;
import com.mindtree.events.services.EventService;
import com.mindtree.events.services.serviceimpl.EventServiceImpl;

@EnableWebMvc
@CrossOrigin
@Controller
public class EventController {
	
	@RequestMapping("/addrace")
	public String display()
	{
		return "display";
	}
	
	@RequestMapping(value="/addrace1",method=RequestMethod.POST)
	public String display1(HttpServletRequest request,HttpServletResponse response)
	{
		String date=request.getParameter("d1");
		String grandprix=request.getParameter("t1");
		String circuit=request.getParameter("t2");
		String country=request.getParameter("t3");
		EventService es=new EventServiceImpl();
		es.addEvent(date,grandprix,circuit,country);
		
		return "display2";
	}
	
	@RequestMapping(value="/addrace2",method=RequestMethod.GET)
	@ResponseBody
	public List<Event1> display2()
	{
		EventService es=new EventServiceImpl();
		List<Event1>list1= es.display();
		return list1;
	}

}

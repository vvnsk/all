package com.mindtree.events.services;

import java.util.List;

import com.mindtree.events.entities.Event1;

public interface EventService {

	void addEvent(String date, String grandprix, String circuit, String country);

	List<Event1> display();
	
	
	

}

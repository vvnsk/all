package com.mindtree.events.services.serviceimpl;

import java.util.List;

import com.mindtree.events.dao.EventDAO;
import com.mindtree.events.dao.DAOImpl.EventDAOImpl;
import com.mindtree.events.entities.Event1;
import com.mindtree.events.services.EventService;

public class EventServiceImpl implements EventService{

	public void addEvent(String date, String grandprix, String circuit, String country)
	{
		Event1 e=new Event1();
		e.setCountry(country);
		e.setCircuit(circuit);
		e.setDate(date);
		e.setGrandprix(grandprix);
		
		EventDAO ed=new EventDAOImpl();
		ed.addRace(e);
		
		
	}

	public List<Event1> display() 
	{
		EventDAO ed=new EventDAOImpl();
		List<Event1>list1=ed.display();
		return list1;
		
	}

}

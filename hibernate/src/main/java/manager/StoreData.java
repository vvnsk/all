package manager;

import java.util.Scanner;

import org.hibernate.Session;  
import org.hibernate.SessionFactory;  
import org.hibernate.Transaction;  
import org.hibernate.cfg.Configuration;

import dao.StudentEntityDAO;
import dao.StudentEntityDAOImpl;
import hibernate.StudentEntity;  

public class StoreData
{
	public static void main(String[] args)
	{
		Scanner sc=new Scanner(System.in);
		StudentEntity e=new StudentEntity();
		System.out.println("enter id");
		int id=sc.nextInt();
		e.setId(id);
		System.out.println("enter name");
		String name=sc.next();
		e.setName(name);
		System.out.println("enter department");
		String dept=sc.next();
		e.setDepartment(dept);
		System.out.println("enter college");
		String college=sc.next();
		e.setCollege(college);
		StudentEntityDAO sed=new StudentEntityDAOImpl();
		sed.addStudent(e);    
	}  
	
	
	

}

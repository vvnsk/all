package exception;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import exception.Exception1.EmployeeException;

public class EmployeeCollection 
{
	public static void main(String[] args) 
	{
		List<Employee> employeeList = new ArrayList<Employee>();
		System.out.println("1.Add Employee");
		System.out.println("2.Display Employees");
		System.out.println("3.Exit");
		System.out.println("Enter your choice:");
		Scanner scan=new Scanner(System.in);
		int choice=scan.nextInt();
		while(choice!=3)
		{
			if(choice==1)
			{
				try{
				Employee e=new Employee();
				int id;
				String name,dept;
				double sal;
				System.out.println("Enter Employee Name:");
				
				name=scan.next();
				
				for(int i=0;i<name.length();i++)
				{
					char ch=name.charAt(i);
					if(!((ch>='A' && ch<='Z')||(ch>='a' && ch<='z')||(ch==' ')))
					{
						throw new EmployeeException("Invalid name");
					}
				}
				e.setEmployeeName(name);
				System.out.println("Enter Employee Id:");
				id=scan.nextInt();
				if(!(id>0 && id<100))
					throw new EmployeeException("Invalid Id");
				e.setEmployeeId(id);
				System.out.println("Enter Employee Department:");
				dept=scan.next();
				for(int i=0;i<dept.length();i++)
				{
					char ch=dept.charAt(i);
					if(!((ch>='A' && ch<='Z')||(ch>='a' && ch<='z')||(ch==' ')))
					{
						throw new EmployeeException("Invalid department name");
					}
				}
				e.setEmployeeDepartment(dept);
				System.out.println("Enter Employee Salary:");
				sal=scan.nextDouble();
				if(sal<10000||sal>50000)
					throw new EmployeeException("Invalid Salary");
				e.setEmployeeSalary(sal);
				employeeList.add(e);
				}
				catch(EmployeeException s)
				{
					System.out.println(s);
					System.out.println("Please Enter valid details again");
				}
				
			}
			else
			{
				if(employeeList.isEmpty())
				{
					System.out.println("No details to display!!!");
				}
				for(Employee e : employeeList)
				{
					System.out.println(e);
				}
			}
			System.out.println("Enter your choice:");
			choice=scan.nextInt();
		}
		scan.close();
	}



}

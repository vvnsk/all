package dao;

import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import hibernate.StudentEntity;

public class StudentEntityDAOImpl implements StudentEntityDAO
{

	public void addStudent(StudentEntity e) {
		Configuration cfg=new Configuration();  
	    cfg.configure("hibernate.cfg.xml");
	    SessionFactory factory=cfg.buildSessionFactory();  
	    Session session=factory.openSession();  
	    Transaction t=session.beginTransaction();  
	    session.persist(e);  
	    t.commit();  
	    session.close();  
	    System.out.println("successfully saved");
		
	}
	

}

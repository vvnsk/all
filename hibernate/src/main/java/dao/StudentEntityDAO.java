package dao;

import hibernate.StudentEntity;

public interface StudentEntityDAO {

	public void addStudent(StudentEntity e);
	
}

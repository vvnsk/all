package com.mindtree.playerauction.entities;

import javax.persistence.*;
import javax.persistence.Table;

@Entity

@Table(name="Team")
public class Team
{
    @Id
	@Column(name="Team_id")
	private int ID;
	
	@Column(name="Team_name")
	private String teamname;
	

	
	public Team() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Team(int iD, String teamname) {
		super();
		ID = iD;
		this.teamname = teamname;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ID;
		result = prime * result + ((teamname == null) ? 0 : teamname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Team other = (Team) obj;
		if (ID != other.ID)
			return false;
		if (teamname == null) {
			if (other.teamname != null)
				return false;
		} else if (!teamname.equals(other.teamname))
			return false;
		return true;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getTeamname() {
		return teamname;
	}

	public void setTeamname(String teamname) {
		this.teamname = teamname;
	}

	@Override
	public String toString() {
		return "Team [ID=" + ID + ", teamname=" + teamname + "]";
	}
	
	
	
   	

}

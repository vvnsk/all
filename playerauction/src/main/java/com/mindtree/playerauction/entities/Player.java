package com.mindtree.playerauction.entities;
import javax.persistence.*;

@Entity
@Table(name="player")
public class Player 
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
  	@Column(name="player_no")
  	private int player_no;
	
  	@Column(name="player_name")
  	private String player_name;
  	
  	@Column(name="category")
	private String category;
	
	@Column(name="highestScore")
	private int highestScore;
	
	@Column(name="bestfigure")
	private String bestfigure;

	public String getPlayer_name() {
		return player_name;
	}

	public void setPlayer_name(String player_name) {
		this.player_name = player_name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getHighestScore() {
		return highestScore;
	}

	public void setHighestScore(int highestScore) {
		this.highestScore = highestScore;
	}

	public String getBestfigure() {
		return bestfigure;
	}

	public void setBestfigure(String bestfigure) {
		this.bestfigure = bestfigure;
	}

	@Override
	public String toString() {
		return "Player [player_no=" + player_no + ", player_name=" + player_name + ", category=" + category
				+ ", highestScore=" + highestScore + ", bestfigure=" + bestfigure + "]";
	}
	
	

}

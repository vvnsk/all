package com.mindtree.playerauction.entities;

import javax.persistence.*;

@Entity
@Table(name="TeamPlayer")
public class TeamPlayer 
{
	@Id
	@Column(name="player_no")
	private int player_no;
	
	@Column(name="team_id")
    private int team_id;

	public int getPlayer_no() {
		return player_no;
	}

	public void setPlayer_no(int player_no) {
		this.player_no = player_no;
	}

	public int getTeam_id() {
		return team_id;
	}

	public void setTeam_id(int team_id) {
		this.team_id = team_id;
	}

	@Override
	public String toString() {
		return "TeamPlayer [player_no=" + player_no + ", team_id=" + team_id + "]";
	}
	
	
}

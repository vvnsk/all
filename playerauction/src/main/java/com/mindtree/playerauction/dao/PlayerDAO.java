package com.mindtree.playerauction.dao;

import java.util.List;

import com.mindtree.playerauction.entities.Player;

public interface PlayerDAO 
{

	void addPlayer(Player p, String teamname);

	List<Object[]> display(String teamname);
	
}

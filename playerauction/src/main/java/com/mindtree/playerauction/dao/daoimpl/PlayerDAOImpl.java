package com.mindtree.playerauction.dao.daoimpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.mindtree.playerauction.dao.PlayerDAO;
import com.mindtree.playerauction.entities.Player;
import com.mindtree.playerauction.entities.Team;
import com.mindtree.playerauction.entities.TeamPlayer;

public class PlayerDAOImpl implements PlayerDAO 
{

	public void addPlayer(Player p, String teamname)
	{
		Configuration cfg=new Configuration();  
	    cfg.configure("hibernate.cfg.xml");
	    SessionFactory factory=cfg.buildSessionFactory();  
	    Session session=factory.openSession();  
	    Transaction t=session.beginTransaction();  
	    session.persist(p);  
	    t.commit();  
	    session.close();  
	    System.out.println("successfully saved");
	    addToTeamPlayer(getPlayerNumber(),getTeamId(teamname));	
	}
	
	

	public void addToTeamPlayer(int playerNumber, int teamId) 
	{
		 
		TeamPlayer tp=new TeamPlayer();
		tp.setPlayer_no(playerNumber);
		tp.setTeam_id(teamId);
		Configuration cfg=new Configuration();  
	    cfg.configure("hibernate.cfg.xml");
	    SessionFactory factory=cfg.buildSessionFactory();  
	    Session session=factory.openSession(); 
	    Transaction t=session.beginTransaction(); 
	    session.persist(tp);  
	    t.commit();  
	    session.close();  
		
	}
	
	public int getTeamId(String teamname) 
	{
		SessionFactory sessionFactory=new Configuration().configure().buildSessionFactory();
		Session session=sessionFactory.openSession();
		session.beginTransaction();
		Query query=session.createQuery("select ID from Team where teamname='"+teamname+"'");
		
		List<Integer> list = query.list();
		return(list.get(0));  
		
	
		
	}
	private int getPlayerNumber()
	{

		SessionFactory sessionFactory=new Configuration().configure().buildSessionFactory();
		Session session=sessionFactory.openSession();
		session.beginTransaction();
		Query query=session.createQuery("select player_no from Player order by player_no desc");
		
		List<Integer> list1 = query.list();
		return(list1.get(0));  
	}



	public List<Object[]> display(String teamname) 
	{
		int team_id=getTeamId(teamname);
		SessionFactory sessionFactory=new Configuration().configure().buildSessionFactory();
		Session session=sessionFactory.openSession();
		session.beginTransaction();
		Query query=session.createQuery("select player_name,category from Player where player_no in(select player_no from TeamPlayer where team_id='"+team_id+"')");
		
		List<Object[]> list2=(List<Object[]>)query.list();
		return list2;
		
	}	
}




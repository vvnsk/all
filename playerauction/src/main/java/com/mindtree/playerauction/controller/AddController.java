package com.mindtree.playerauction.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.mindtree.playerauction.services.PlayerService;
import com.mindtree.playerauction.services.playerserviceimpl.PlayerServiceImpl;



@Controller
public class AddController 

{
	@RequestMapping("/addplayer")
	public String display1()
	{
		return "display";
	}
	@RequestMapping("/addplayer1")
	public String display2(HttpServletRequest request,HttpServletResponse response)
	{
		String player_name=request.getParameter("t1");
		String category=request.getParameter("t2");
		int highestscore=Integer.parseInt(request.getParameter("t3"));
		String bestfigure=request.getParameter("t4");
		String teamname=request.getParameter("t5");
		PlayerService ps=new PlayerServiceImpl();
		ps.addplayerService(player_name, category, highestscore, bestfigure, teamname);
		return null;
	}
	
	@RequestMapping("/displayplayer")
	public String display2()
	{
		return "display2";
	}
	
	@RequestMapping("/addplayer2")
	public ModelAndView display3(HttpServletRequest request,HttpServletResponse response)
	{
		String teamname=request.getParameter("t6");
		PlayerService ps=new PlayerServiceImpl();
	
		List<Object[]> list2=ps.display(teamname);
//		String[] pl=new String[list2.size()];
		
//		for(Object[] employee: list2){
//	         String player_name = (String)employee[0];
//	         String category = (String)employee[1];
//	         System.out.println(player_name);
//	         System.out.println(category); 
//	         pl[i]=player_name;
//	         i++;
//	        
//	     }
		ModelAndView mv=new ModelAndView();
		mv.setViewName("display3");
		mv.addObject("result",list2);
	
		return mv;
		
	}
	@Override
	public String toString() {
		return "AddController [display1()=" + display1() + ", display2()=" + display2() + "]";
	}
	
	
	
	 
	
	

}

package com.mindtree.playerauction.services;

import java.util.List;

public interface PlayerService 
{

	public void addplayerService(String player_name, String category, int highestscore, String bestfigure,String teamname);

	public List<Object[]> display(String teamname);

}

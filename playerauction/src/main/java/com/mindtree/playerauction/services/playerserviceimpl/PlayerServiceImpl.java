package com.mindtree.playerauction.services.playerserviceimpl;

import java.util.List;

import com.mindtree.playerauction.dao.PlayerDAO;
import com.mindtree.playerauction.dao.daoimpl.PlayerDAOImpl;

import com.mindtree.playerauction.entities.Player;
import com.mindtree.playerauction.services.PlayerService;

public class PlayerServiceImpl implements PlayerService
{
	PlayerDAO accessDao=new PlayerDAOImpl();
	public void addplayerService(String player_name, String category, int highestscore, String bestfigure,String teamname)
	{
		Player p=new Player();
		p.setBestfigure(bestfigure);;
		p.setCategory(category);
		p.setHighestScore(highestscore);
		p.setPlayer_name(player_name);
		accessDao.addPlayer(p,teamname);
	}
	public List<Object[]> display(String teamname)
	{
		PlayerDAO pd=new PlayerDAOImpl();
		List<Object[]> list1=pd.display(teamname);
		return list1;
		
	}
	

}

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
<style>           
.blue-button{
    background: #25A6E1;
    filter: progid: DXImageTransform.Microsoft.gradient( startColorstr='#25A6E1',endColorstr='#188BC0',GradientType=0);
    padding:3px 5px;
    color:#fff;
    font-family:'Helvetica Neue',sans-serif;
    font-size:12px;
    border-radius:2px;
    -moz-border-radius:2px;
    -webkit-border-radius:4px;
    border:1px solid #1A87B9;
    margin:auto 0;
}     
table {
  font-family: "Helvetica Neue", Helvetica, sans-serif;
   width: 50%;
}
th {
  background: SteelBlue;
  color: white;
}
 td,th{
                border: 1px gray;
                width: 25%;
                text-align: left;
                padding: 5px 10px;
            }
</style>
<script>

    function validateForm() {
    var fname = document.forms["myForm"]["firstName"].value;
    var b1 = true;
    if (fname == "") {
        fnfnamevalidate("firstName","1");
        b1  = false;
    }
    
    
    var lname = document.forms["myForm"]["lastName"].value;
    var b2 = true;
    if (lname == "") {
        fnlnamevalidate("lastName","2");
        b2  = false;
    }
    
    
    var age = document.forms["myForm"]["age"].value;
    var b3 = true;
    if (age == "") {
        fnagevalidate("age","3");
        b3  = false;
    }
    /* if(age < 0){
        alert("age cannot be negative!");
        return false;
    }
    
    if (age > 0 && age < 14 ){
        alert("your age must be above 14 years!");
        return false;
    } */
 
   
    
    //email validation
    /* var email = document.forms["myForm"]["email"].value;
    function IsValidEmail(email) {
          // var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            var expr = /[a-zA-Z]([a-zA-Z0-9._]*)@([a-zA-Z0-9]+)(([.][a-zA-Z]+)+)/;
           return expr.test(email);
       }; */
       
       var email = document.forms["myForm"]["email"].value; 
       var b4 =true;
        if (email == "") {
            fnemailvalidate("email","4");
            b4  = false;
   } 
   

      
/*       var mobile = document.forms["myForm"]["mobileNo"].value;
      var b5 = true;
        if (mobile == "") {
      fnmobilevalidate("mobile","5");
            b5  = false;
  }  */
  

   
   var enterPassword = document.forms["myForm"]["enterPassword"].value;
   var b5 = true;
   if (enterPassword == "") {
       fnpasswordvalidate("enterPassword","6");
          b5 = false;
      } 
   
   var reEnterPassword = document.forms["myForm"]["reEnterPassword"].value;
   
   var b5 = true;
   if (reEnterPassword == "") {
       fnmatchPassword("enterPassword","reEnterPassword","7");
          b6 = false;
      } 
   
   if(b1 && b2 && b3 && b4  && b6){
  // if(b1 && b2 && b3 && b4 && b5 && b6){
       return true;
   }else{
       return false;
   }
   
   function checkPassword(enterPassword)
   {
     var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
     return re.test(enterPassword);
   };
   
   if(enterPassword != "" && enterPassword == reEnterPassword) {
          if(!checkPassword(enterPassword)) {
            alert(" Password must contain at least one number, one lowercase and one uppercase letter and at least 8 characters that are letters or numbers ");
            document.forms["myForm"]["enterPassword"].focus();
            return false;
          }
        } else {
          alert("Please check that you have entered and confirmed your password!");
          document.forms["myForm"]["enterPassword"].focus();
          return false;
        }
    
    
     }


     function fnclear(id,lbl){
        document.getElementById(id).value="";
     }



    
    function isAlphabet(name){
         var expr = /^[a-zA-Z][a-zA-Z][a-zA-Z]+$/;
          return expr.test(name);
    }



   // var x=document.getElementById("firstName");
    function fnfnamevalidate(id,lbl){
      var fname=document.getElementById(id).value;
        x=document.getElementById(lbl);
        if(document.getElementById(id).value!=""){
            if(!isAlphabet(fname)){
                x.innerHTML = "<front color=red>*Name should contain only alphabets and minimum of 3 characters </front>";
                 
                fnclear(id,lbl);
            }else
            x.innerHTML="";
        }else
        x.innerHTML="<front color=red>*The field shoud have some value</front>";
        x.style.background = "yellow";
     }


     //var y=document.getElementById("lastName");
    function fnlnamevalidate(id,lbl){
      var lname=document.getElementById(id).value;
        y=document.getElementById(lbl);
        if(document.getElementById(id).value!=""){
            if(!isAlphabet(lname)){
                y.innerHTML = "<front color=red>*Name should contain only alphabets and minimum of 3 characters </front>";
                
                fnclear(id,lbl);
            }else
            y.innerHTML="";
        }else
        y.innerHTML="<front color=red>*The field shoud have some value</front>";
         y.style.background = "yellow";
     }


       function fnagevalidate(id,lbl){
      var age=document.getElementById(id).value;
        y=document.getElementById(lbl);
        if(document.getElementById(id).value!=""){
            if(isNaN(age)==true){
                y.innerHTML = "<front color=red>*Age must be a number </front>";
                fnclear(id,lbl);
            }else if(age < 0){
                 y.innerHTML = "<front color=red>*Age cannot be negative </front>";
                fnclear(id,lbl);
                
            }else if(age <14 || age > 90){
                 y.innerHTML = "<front color=red>*Age must be between 14 and 90 </front>";
                fnclear(id,lbl);  
            }
            else
            y.innerHTML="";
        }else
        y.innerHTML="<front color=red>*The field shoud have some value</front>";
         y.style.background = "yellow";
     }


        function isValidEmail(email) {
          // var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            var expr = /[a-zA-Z]([a-zA-Z0-9._]*)@([a-zA-Z0-9]+)(([.][a-zA-Z]+)+)/;
           return expr.test(email);
       }


 function fnemailvalidate(id,lbl){
      var email=document.getElementById(id).value;
        mess=document.getElementById(lbl);
        if(document.getElementById(id).value!=""){
            if(!isValidEmail(email)){
                mess.innerHTML = "<front color=red>*Invalid Email!</front>";
                fnclear(id,lbl);
            }
            else
            mess.innerHTML="";
        }else
        mess.innerHTML="<front color=red>*The field shoud have some value</front>";
         mess.style.background = "yellow";
     }

      function isValidMobile(mobile) {
          var expr = /(7|8|9)\d{9}$/;
      
          return expr.test(mobile);
      }


 function checkPassword(password)
   {
     var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
     return re.test(password);
   }

function fnpasswordvalidate(id,lbl){
     var password=document.getElementById(id).value;
        messg=document.getElementById(lbl);
        if(document.getElementById(id).value!=""){
            if(!checkPassword(password)){
                messg.innerHTML = "<front color=red>*Password must contain at least one number, one lowercase and one uppercase letter and at least 8 characters that are letters or numbers </front>";
                fnclear(id,lbl);
            }
            else
            messg.innerHTML="";
        }else
        messg.innerHTML="<front color=red>*The field shoud have some value</front>";
         messg.style.background = "yellow";
     }
     
function fnmatchPassword(id1,id2,lbl){
    var password=document.getElementById(id1).value;
    var repassword=document.getElementById(id2).value;
       messg=document.getElementById(lbl);
       if(document.getElementById(id2).value!=""){
           if(password!=repassword){
               messg.innerHTML = "<front color=red>*Password must be same</front>";
               fnclear(id2,lbl);
           }
           else
           messg.innerHTML="";
       }else
       messg.innerHTML="<front color=red>*The field shoud have some value</front>";
        messg.style.background = "yellow";
    }
     
    
    
    


    </script>
</head>
<body>

<form method="post" name="myForm" modelAttribute="registeration" onSubmit="return validateForm()"  action="/ferari/addRegisteration">
<table>
        <tr>
            <th colspan="2">User Registeration</th>
        </tr>
        
        <tr><td>First Name:</td><td><input type="text" name="firstName" id="firstName"  onblur='fnfnamevalidate("firstName","1")'/></td> <td><div id="1" value=""> </div></td></tr>  
        <tr><td>Last Name:</td><td><input type="text" name="lastName"   id="lastName"  onblur='fnlnamevalidate("lastName","2")'/></td> <td><div id="2" value=""> </div></td></tr>  
        <tr><td>Age:</td><td><input type="text" name="age" id="age"  onblur='fnagevalidate("age","3")'/></td> <td><div id="3" value=""> </div></td></tr>
        <tr><td>Email:</td><td><input type="email" name="email" id="email" onblur='fnemailvalidate("email","4")'/></td> <td><div id="4" value=""> </div></td></tr>  
        
        <tr><td>Gender</td><td>
        <input type="radio" name="gender" value="Male" checked> Male
        <input type="radio" name="gender" value="Female"> Female </td></tr>
       
        
        <tr><td>Enter Password:</td> <td> <input type="password" name="enterPassword" id="enterPassword"  onblur='fnpasswordvalidate("enterPassword","6")'/></td> <td><div id="6" value=""> </div></td></tr> 
        
        <tr><td>Confirm Password:</td> <td> <input type="password" name="reEnterPassword"id="reEnterPassword"  onblur='fnmatchPassword("enterPassword","reEnterPassword","7")'/></td> <td><div id="7" value=""> </div></td></tr> 
    
        <tr>
            <td colspan="2"><input type="submit"
                value="submit" /></td>
        </tr>
    </table> 
</form>


</body>
</html>
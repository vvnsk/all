package com.mindtree.lottery.controller;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.mindtree.lottery.dao.PersonDAO;
import com.mindtree.lottery.dao.daoimpl.Email;
import com.mindtree.lottery.dao.daoimpl.PersonDAOImpl;
import com.mindtree.lottery.services.PersonService;
import com.mindtree.lottery.services.playerserviceimpl.PersonServiceImpl;



@Controller
public class AddController1 {
	
	@RequestMapping("/addperson")
	public String display()
	{
		return "display";
	}
	
	@RequestMapping(value="/addperson1",method=RequestMethod.POST)
	public String display1(HttpServletRequest request,HttpServletResponse response)
	{
		String name=request.getParameter("t1");
		String address=request.getParameter("t2");
		String age=request.getParameter("t4");
		String mail=request.getParameter("t3");
		String gender=request.getParameter("gen");
		String mobile=request.getParameter("t5");
     	PersonService ps=new PersonServiceImpl();
     	
     	ps.addPerson(name,address,age,mail,gender,mobile);
     	System.out.println("into controll");
     	System.out.println(ps);
     	return "lottery";
	}
	
	@RequestMapping("/lottery")
	public String lottery()
	{
		PersonDAO lot=new PersonDAOImpl();
	    lot.lottery();
	    return "lottery";
	}
	@RequestMapping("/lotterydate")
	public String lotterydate()
	{
		
		return "lotterydate";
	}
	@RequestMapping("/add")
	public void lotterydate1(HttpServletRequest request,HttpServletResponse response)
	{
		String date=request.getParameter("date");
		PersonDAO dat=new PersonDAOImpl();
		dat.lotterydate(date);
	}
	
	@RequestMapping("/winners")
	public String winners()
	{
		
		return "winners";
		
	}
	
	@RequestMapping("/winners1")
	public ModelAndView  winners1(HttpServletRequest request,HttpServletResponse response)
	{
		String date=request.getParameter("date");
		PersonDAO dat=new PersonDAOImpl();
		List l2=dat.lotterywinners(date);
		ModelAndView mv=new ModelAndView();
		mv.setViewName("winners1");
		mv.addObject("result",l2);
	
		return mv;
	
		
	}
	
	@RequestMapping("/email")
	public void email()
	{
		System.out.println("this is email controller");
		Email em=new Email();
		em.email();
		
	}
	
	
	
}

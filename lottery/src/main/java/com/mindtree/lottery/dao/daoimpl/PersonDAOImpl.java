package com.mindtree.lottery.dao.daoimpl;


import java.util.Arrays;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import org.hibernate.cfg.Configuration;


import com.mindtree.lottery.dao.PersonDAO;
import com.mindtree.lottery.entities.Person;


public class PersonDAOImpl implements PersonDAO
{


	public void addplayer(Person p)
	{
		Configuration cfg=new Configuration();  
	    cfg.configure("hibernate.cfg.xml");
	    SessionFactory factory=cfg.buildSessionFactory();  
	    Session session=factory.openSession();  
	    Transaction t=session.beginTransaction();  
	    session.persist(p);  
	    t.commit();  
	    session.close();  
	    System.out.println("successfully saved");
	    
	}

	

	private int lastid() 
	{
		SessionFactory sessionFactory=new Configuration().configure().buildSessionFactory();
		Session session=sessionFactory.openSession();
		session.beginTransaction();
		Query query=session.createQuery("select id from Person order by id desc");
		
		List<Integer> list1 = query.list();
		return(list1.get(0));  
	}

	public void lottery()
	{
		
		int id=lastid();
	    int i=0;
	    int[] numbers = new int[4]; 
		while(i!=4)
	    {
			
			     int  r = (int)(Math.random()*id + 1);
			     Arrays.sort(numbers);
			     int r2=Arrays.binarySearch(numbers,r);
			     if(r2<0)
			     {
			     int r1=check(r);
			      if((r1!=1) && (r>0))
			      {
			    	  System.out.println("hi"+r);
			    	  numbers[i]=r;
			    	  System.out.println("the value is "+numbers[i]+"the index is"+i);
			    	  i++;
			      }
			     }
	    }
//		for(i=0;i<numbers.length;i++)
//		{
//			System.out.println(numbers[i]);	
//		}
//		
		
	}
	private int check(int r) 
	{
		SessionFactory sessionFactory=new Configuration().configure().buildSessionFactory();
		Session session=sessionFactory.openSession();
		session.beginTransaction();
		Query query=session.createQuery("select flag from Person where id='"+r+"'");
		List<Integer> list1 = query.list();
		return(list1.get(0));  
	}



	public void lotterydate(String date) 
	{
		int id=lastid();
	    int i=0;
	    int[] numbers = new int[4]; 
		while(i!=4)
	    {
			
			     int  r = (int)(Math.random()*id + 1);
			     Arrays.sort(numbers);
			     int r2=Arrays.binarySearch(numbers,r);
			     if(r2<0)
			     {
			     int r1=check(r);
			      if((r1==0) && (r>0))
			      {
			    	  update(date,r);
			    	  i++;
			      }
			     }
	    }

		
		
	}

	private void update(String date, int r) 
	{
		int flag=1;
		Configuration cfg=new Configuration();  
	    cfg.configure("hibernate.cfg.xml");
	    SessionFactory factory=cfg.buildSessionFactory();  
	    Session session=factory.openSession();  
		session.getTransaction().begin();
		String hql = "update Person set date ='" + date + "',flag='"+flag+"'  where id ='" + r + "'";
         
		Query query=session.createQuery(hql);
		query.executeUpdate();
		session.getTransaction().commit();
		
	}

	public List lotterywinners(String date)
	
	{
		SessionFactory sessionFactory=new Configuration().configure().buildSessionFactory();
		Session session=sessionFactory.openSession();
		session.beginTransaction();
		Query query=session.createQuery("select name,email,mobile from Person where date='"+date+"'");
		List l2=query.list();
//		Iterator itr=l2.iterator();
//		while(itr.hasNext())
//		{
//			Object a[]=(Object[])itr.next();
//			System.out.println(a[0]+"\t"+a[1]+"\t"+a[2]);
//		}
//		
		return l2;
				
	}

}

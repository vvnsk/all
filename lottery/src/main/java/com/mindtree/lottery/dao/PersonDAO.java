package com.mindtree.lottery.dao;

import java.util.List;

import com.mindtree.lottery.entities.Person;

public interface PersonDAO {

	void addplayer(Person p);
	
	void lottery();

	void lotterydate(String date);

	List lotterywinners(String date);

}

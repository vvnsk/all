package com.mindtree.queen.service.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mindtree.queen.dao.SongDAO;
import com.mindtree.queen.dao.entity.Song;
import com.mindtree.queen.service.SongService;

@Service
public class SongServiceImpl implements SongService{
	
	@Autowired
	private SongDAO songDAO;
	
	public void setSongDAO(SongDAO songDAO) {
		this.songDAO = songDAO;
	}

	@Override
	@Transactional
	public void addSong(Song song) {
		this.songDAO.addSong(song);
		
	}

	@Override
	@Transactional
	public void deleteSong(int songId) {
		this.songDAO.deleteSong(songId);
		
	}

	@Override
	@Transactional
	public List<Song> getALLSongs() {
		
		return songDAO.getALLSongs();
	}

	@Override
	@Transactional
	public Song getSong(int songId) {
		System.out.println("service id = "+songId);
		return songDAO.getSong(songId);
	}

	@Override
	@Transactional
	public Song updateSongDetail(Song song) {
		return this.songDAO.updateSongDetail(song);
		
	}

}

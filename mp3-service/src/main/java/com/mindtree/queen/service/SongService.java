package com.mindtree.queen.service;

import java.util.List;

import com.mindtree.queen.dao.entity.Song;
/**
 * 
 * @author M1027284
 *
 */
public interface SongService {
	
	/**
	 * Add Song to database.
	 * @param song
	 */
	void addSong(Song song);
	
	/**
	 * Deleting song from database.
	 * @param songId
	 */
	void deleteSong(int songId);
	 
	/**
	 * Fetch all songs from the database.
	 * @return all songs
	 */
	List<Song> getALLSongs();
	
	/**
	 * Fetch a particular song on user request.
	 * 
	 * @param songId
	 * @return Song detail
	 */
	Song getSong(int songId);
	
	/**
	 *  Update the details of song
	 * @param songId
	 */
	Song updateSongDetail(Song song);
	
	

}

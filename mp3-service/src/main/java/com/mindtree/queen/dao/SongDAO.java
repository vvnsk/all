package com.mindtree.queen.dao;

import java.util.List;

import com.mindtree.queen.dao.entity.Song;

public interface SongDAO {
	
	/**
	 * Add Song to database.
	 * @param song
	 */
	void addSong(Song song);
	
	/**
	 * Deleting song from database.
	 * @param songId
	 */
	void deleteSong(int songId);
	 
	/**
	 * Fetch all songs from the database.
	 * @return all songs
	 */
	List<Song> getALLSongs();
	
	/**
	 * Fetch a particular song on user request.
	 * 
	 * @param songId
	 * @return Song detail
	 */
	Song getSong(int songId);
	
	/**
	 *  Update the details of a song
	 * @param songId
	 */
	Song updateSongDetail(Song song);

}
